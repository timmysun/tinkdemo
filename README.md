# TinkDemo

###Quiz 1 
Use recursive method to calculate the expression. 
Code is written in java at app\src\main\java\com\timmy\tinkdemo\util\Quiz1Utils.java

###Quiz 2
Write in kotlin. Use Model-(ViewModel)-View

###Quiz 3
1. Use Mock server to test retrofit and WebApiClient
Code is at app\src\androidTest\java\com\timmy\tinkdemo\DataPoolTest.kt
2. Use Stub DataPool to test the data shown on Activity
Code is at app\src\androidTest\java\com\timmy\tinkdemo\MainActivityTest.kt