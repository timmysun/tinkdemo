package com.timmy.tinkdemo

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.timmy.tinkdemo.model.DataPool
import com.timmy.tinkdemo.model.WebApiClient
import com.timmy.tinkdemo.util.Constants
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONObject
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File


/**
 * DataPool test
 */
@RunWith(AndroidJUnit4::class)
class DataPoolTest {

    private lateinit var mMockWebServer: MockWebServer

    @Before
    @Throws(Exception::class)
    fun setUp() {
        //mock a web server
        mMockWebServer = MockWebServer()
        mMockWebServer.start(8080)

        //set mocked server to WebApiClient
        WebApiClient.instance = MockWebApiClient()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        mMockWebServer.shutdown()
    }

    @Test
    fun testRetrieveImageData() {

        //setup fake data
        val responseJson = JSONObject("{\"result\":{\"results\":[{\"F_Pic01_URL\":\"http://www.zoo.gov.tw/iTAP/04_Plant/Lythraceae/subcostata/subcostata_1.jpg\",\"F_Pic02_URL\":\"\",\"F_Pic03_URL\":\"\",\"F_Name_Ch\":\"九芎\",\"F_Brief\":\"分布於中低海拔森林及長江以南的地區，為台灣的原生樹種。主要生長在潮濕的崩塌地，有吸水保持土壤之特性，所以是良好的水土保持樹種。\",\"F_Pic04_URL\":\"\"}]}}")
        val result = responseJson.getJSONObject("result").getJSONArray("results").getJSONObject(0)
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(200)
        mockResponse.setBody(responseJson.toString())

        //enqueue to mocked server
        mMockWebServer.enqueue(mockResponse)

        val context = InstrumentationRegistry.getTargetContext()

        //delete cache file to make retrieveImageData call to server
        File(context.cacheDir, Constants.IMAGE_DATA_CACHE_FILE).deleteOnExit()
        DataPool.instance.retrieveImageData(context)

        //wait for data complete
        Thread.sleep(3000)

        //check data
        val data = DataPool.instance.getData(context.getString(R.string.section_city_guide), context)
        Assert.assertEquals(data.size, 1)
        Assert.assertTrue(data[0].mTitle!!.contentEquals(result.getString("F_Name_Ch")))
        Assert.assertTrue(data[0].mMessage!!.contentEquals(result.getString("F_Brief")))
        Assert.assertTrue(data[0].mImageUrl!!.contentEquals(result.getString("F_Pic01_URL")))
    }
}
