package com.timmy.tinkdemo

import android.content.Context
import com.timmy.tinkdemo.event.DataUpdateEvent
import com.timmy.tinkdemo.model.DataPool
import com.timmy.tinkdemo.model.ImageData
import org.greenrobot.eventbus.EventBus

/**
 * A Stub DataPool to test MainActivity
 */
class DataPoolStub: DataPool() {

    private val mStubData = ArrayList<ImageData>()

    override fun getData(key: String, context: Context?): ArrayList<ImageData> {
        return mStubData
    }

    override fun retrieveImageData(context: Context) {
        val data = ArrayList<ImageData>()
        for (i in 1..10) {
            data.add(ImageData("Title$i", "Message$i", "Image$i"))
        }
        mStubData.addAll(data.shuffled())
        EventBus.getDefault().post(DataUpdateEvent())
    }
}