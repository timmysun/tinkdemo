package com.timmy.tinkdemo

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.timmy.tinkdemo.model.DataPool
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.anything
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, false)

    private lateinit var mSubData: DataPoolStub

    @Before
    @Throws(Exception::class)
    fun setUp() {
        Intents.init()

        //set up a stub DataPool
        mSubData = DataPoolStub()
        DataPool.instance = mSubData

        //launch activity
        activityRule.launchActivity(null)
        intended(hasComponent(MainActivity::class.java.name))
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        Intents.release()
    }

    @Test
    fun testCityGuideFragment() {
        checkData()
    }

    @Test
    fun testShopFragment() {
        //swipe to Shop Fragment
        onView(withId(R.id.container)).perform(swipeLeft())
        Thread.sleep(2000)
        checkData()
    }

    @Test
    fun testEatFragment() {
        //swipe to Eat Fragment
        onView(withId(R.id.container)).perform(swipeLeft())
        Thread.sleep(2000)
        onView(withId(R.id.container)).perform(swipeLeft())
        Thread.sleep(2000)
        checkData()
    }

    /**
     * check display data is the same with stub data
     */
    private fun checkData() {
        for (i in 0..9) {
            if (i % 2 == 0) {
                onData(anything()).inAdapterView(allOf(withId(R.id.section_list_view), isCompletelyDisplayed()))
                    .atPosition(i).onChildView(withId(R.id.title))
                    .check(ViewAssertions.matches(ViewMatchers.withText(mSubData.getData("whatever", null)[i].mTitle)))

                onData(anything()).inAdapterView(allOf(withId(R.id.section_list_view), isCompletelyDisplayed()))
                    .atPosition(i).onChildView(withId(R.id.desc))
                    .check(ViewAssertions.matches(ViewMatchers.withText(mSubData.getData("whatever", null)[i].mMessage)))
            }
        }
    }
}