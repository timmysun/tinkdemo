package com.timmy.tinkdemo

import com.timmy.tinkdemo.model.WebApiClient
import com.timmy.tinkdemo.model.server.Image
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MockWebApiClient: WebApiClient() {

    override fun getImagesRx(): Single<Response<Image>> {
        return Retrofit.Builder()
            .baseUrl("http://localhost:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(OkHttpClient())
            .build()
            .create(WebApiClient.WebApi::class.java).getImageInfo()
    }
}