package com.timmy.tinkdemo

import com.timmy.tinkdemo.util.Utils
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.io.Serializable

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UtilsUnitTest {

    //A fake object for test serializable
    data class FakeObj(
        val mFakeString: String
    ) : Serializable {
        companion object {
            private const val serialVersionUID: Long = 876323262645176999L
        }
    }

    @Test
    fun testSaveAndLoadFromCache() {
        //create fake obj
        val obj = FakeObj("whatever")
        //save to cache file
        val cache = File("temp")
        Utils.saveToFile(cache, obj)
        //load from cache file
        val result = Utils.loadFromFile(cache) as FakeObj
        //check data is the same
        assertTrue(obj.mFakeString.contentEquals(result.mFakeString))
    }
}
