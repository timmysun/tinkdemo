package com.timmy.tinkdemo

import com.timmy.tinkdemo.util.Quiz1Utils
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*


class Quiz1UtilsUnitTest {

    @Test
    fun combineToValueConst() {
        //Test no operator case
        val result = Quiz1Utils.combineToValue("100", 100)
        assertEquals(result.size, 0)

        //Test known case
        val result1 = Quiz1Utils.combineToValue("1001", 101)
        assertEquals(result1.size, 1)
        assertTrue(result1[0]!!.contentEquals("100 + 1"))
    }

    @Test
    fun combineToValueRandom() {
        val random = Random()
        //Test 10 times
        for (i in 1..10) {
            //Random input 0 ~ INT MAX
            val randomInput = random.nextInt() and Integer.MAX_VALUE
            //Random value 0 ~ 100
            val randomValue = random.nextInt(100)
            //Test answer
            val result = Quiz1Utils.combineToValue(randomInput.toString(), randomValue.toLong())
            //Check every answer is correct
            for (exp in result) {
                assertEquals(randomValue.toLong(), calculateValue(exp))
            }
        }
    }

    private fun calculateValue(exp: String): Long {
        val list = exp.split(" ")
        var value: Long = 0
        var lastOp = "+"
        for (item in list) {
            when (item) {
                "+" -> lastOp = "+"
                "-" -> lastOp = "-"
                else -> {
                    val vItem = item.toLong()
                    value += when (lastOp) {
                        "+" -> vItem
                        "-" -> -vItem
                        else -> return -1
                    }
                }
            }
        }
        return value
    }
}