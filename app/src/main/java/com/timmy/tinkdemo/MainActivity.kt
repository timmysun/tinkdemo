package com.timmy.tinkdemo

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.timmy.tinkdemo.helper.Quiz1DialogHelper
import com.timmy.tinkdemo.model.DataPool
import com.timmy.tinkdemo.ui.adapter.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mQuiz1Dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //set action bar
        setSupportActionBar(toolbar)

        //set info click listener
        fab.setOnClickListener {
            showQuiz1Dialog()
        }

        //set fragment view pager adapter
        container.adapter = SectionsPagerAdapter(supportFragmentManager, this)

        //combine tab and view pager
        tabs.setupWithViewPager(container)

        //retrieve image data
        DataPool.instance.retrieveImageData(this)
    }

    private fun showQuiz1Dialog() {
        if (mQuiz1Dialog == null)
            //get dialog instance and setup listener
            mQuiz1Dialog = showQuiz1Dialog {
                onNegativeClickListener {
                    mQuiz1Dialog?.dismiss()
                }
            }
        // show dialog
        mQuiz1Dialog?.show()
    }

    private inline fun AppCompatActivity.showQuiz1Dialog(func: Quiz1DialogHelper.() -> Unit): AlertDialog =
        Quiz1DialogHelper(this).apply {
            func()
        }.create()
}
