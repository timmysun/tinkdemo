package com.timmy.tinkdemo.ui.fragment

import com.timmy.tinkdemo.R

class ShopFragment : BaseFragment() {
    override val mKeyWord: String
        get() = getString(R.string.section_shop)
}