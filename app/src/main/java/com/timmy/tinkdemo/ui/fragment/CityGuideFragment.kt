package com.timmy.tinkdemo.ui.fragment

import com.timmy.tinkdemo.R

class CityGuideFragment : BaseFragment() {
    override val mKeyWord: String
        get() = getString(R.string.section_city_guide)
}