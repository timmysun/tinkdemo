package com.timmy.tinkdemo.ui.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.databinding.ListItemImageOnlyBinding
import com.timmy.tinkdemo.databinding.ListItemImageTextBinding
import com.timmy.tinkdemo.event.DataUpdateEvent
import com.timmy.tinkdemo.model.DataPool
import com.timmy.tinkdemo.model.ImageData
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ListViewAdapter(data: ArrayList<ImageData>, key: String, context: Context?) : BaseAdapter() {

    companion object {
        private const val TYPE_IMAGE = 0
        private const val TYPE_IMAGE_TEXT = 1
        private const val TYPE_COUNT = 2
    }

    init {
        EventBus.getDefault().register(this)
    }

    private val mContext = context
    private val mKeyword = key
    private val mData = data

    override fun getCount() = mData.size

    override fun getItem(position: Int) = mData[position]

    override fun getItemId(position: Int) = position.toLong()

    override fun getItemViewType(position: Int) = if (position % 2 == 0) TYPE_IMAGE_TEXT else TYPE_IMAGE

    override fun getViewTypeCount() = TYPE_COUNT

    override fun getView(position: Int, convertView: View?, parent: ViewGroup) = when (getItemViewType(position)) {
        TYPE_IMAGE_TEXT -> {
            val binding =
                if (convertView == null) DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.list_item_image_text,
                    parent, false
                ) as ListItemImageTextBinding
                else DataBindingUtil.getBinding<ListItemImageTextBinding>(convertView)
            binding!!.imageData = mData[position]
            binding.root
        }
        TYPE_IMAGE -> {
            val binding =
                if (convertView == null) DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.list_item_image_only,
                    parent, false
                ) as ListItemImageOnlyBinding
                else DataBindingUtil.getBinding<ListItemImageOnlyBinding>(convertView)
            binding!!.imageData = mData[position]
            binding.root
        }
        else -> convertView!!
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    fun onDataUpdate(event: DataUpdateEvent) {
        mData.addAll(DataPool.instance.getData(mKeyword, mContext))
        notifyDataSetChanged()
    }
}