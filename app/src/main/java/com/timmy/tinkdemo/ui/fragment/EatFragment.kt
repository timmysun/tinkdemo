package com.timmy.tinkdemo.ui.fragment

import com.timmy.tinkdemo.R

class EatFragment : BaseFragment() {
    override val mKeyWord: String
        get() = getString(R.string.section_eat)
}