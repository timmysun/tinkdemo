package com.timmy.tinkdemo.ui.adapter

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.ui.fragment.CityGuideFragment
import com.timmy.tinkdemo.ui.fragment.EatFragment
import com.timmy.tinkdemo.ui.fragment.ShopFragment

class SectionsPagerAdapter(fm: FragmentManager?, context: Context) : FragmentPagerAdapter(fm) {

    private val mContext = context

    override fun getItem(position: Int) = when (position) {
        0 -> CityGuideFragment()
        1 -> ShopFragment()
        2 -> EatFragment()
        else -> null
    }

    override fun getCount() = 3

    override fun getPageTitle(position: Int) = when (position) {
        0 -> mContext.getString(R.string.section_city_guide)
        1 -> mContext.getString(R.string.section_shop)
        2 -> mContext.getString(R.string.section_eat)
        else -> super.getPageTitle(position)
    }
}