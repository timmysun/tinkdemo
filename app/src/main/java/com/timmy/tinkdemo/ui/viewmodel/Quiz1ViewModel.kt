package com.timmy.tinkdemo.ui.viewmodel

import android.content.Context
import android.databinding.ObservableField
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.util.Quiz1Utils

class Quiz1ViewModel(context: Context) {

    private val mContext = context
    val mOutput = ObservableField<String>()

    fun calculateQuiz1(input: String, value: Long) {
        val results = Quiz1Utils.combineToValue(input, value)

        //set result
        mOutput.set(
            when (results.size) {
                0 -> mContext.getString(R.string.string_quiz1_no_result)
                else -> results.joinToString(separator = System.lineSeparator())
            }
        )
    }
}