package com.timmy.tinkdemo.ui.fragment

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.databinding.FragmentMainBinding
import com.timmy.tinkdemo.model.DataPool
import com.timmy.tinkdemo.ui.adapter.ListViewAdapter

abstract class BaseFragment : Fragment() {
    abstract val mKeyWord: String

    private var mAdapter: ListViewAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //init data binding
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_main, container, false) as FragmentMainBinding
        mAdapter = ListViewAdapter(DataPool.instance.getData(mKeyWord, context), mKeyWord, context)
        binding.adapter = mAdapter
        return binding.root
    }

}