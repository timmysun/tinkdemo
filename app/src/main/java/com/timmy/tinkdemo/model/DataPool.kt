package com.timmy.tinkdemo.model

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.event.DataUpdateEvent
import com.timmy.tinkdemo.model.server.Image
import com.timmy.tinkdemo.model.server.ImageResult
import com.timmy.tinkdemo.util.Constants
import com.timmy.tinkdemo.util.Utils
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import retrofit2.Response
import java.io.File


open class DataPool {

    private val mDisposables = CompositeDisposable()
    private val mCityGuideData = ArrayList<ImageData>()
    private val mEatData = ArrayList<ImageData>()
    private val mShopData = ArrayList<ImageData>()

    private object Holder {
        val INSTANCE = DataPool()
    }

    companion object {
        var instance =  Holder.INSTANCE
        private const val TAG = "DataPool"
    }

    /**
     * load data cache with Single
     */
    private fun loadCache(context: Context): Single<ImageResult> {
        return Single.create<ImageResult> { e ->
            val imageData: Any? = Utils.loadFromFile(File(context.cacheDir, Constants.IMAGE_DATA_CACHE_FILE))
            if (imageData == null) {
                e.onError(Throwable())
            } else {
                e.onSuccess(imageData as ImageResult)
            }
        }
    }

    /**
     * get data by key
     */
    open fun getData(key: String, context: Context?): ArrayList<ImageData> {
        return if (context == null) ArrayList() else when (key) {
            context.getString(R.string.section_city_guide) -> mCityGuideData
            context.getString(R.string.section_eat) -> mEatData
            context.getString(R.string.section_shop) -> mShopData
            else -> ArrayList()
        }
    }

    /**
     * retrieve image data
     * if no cache, load from network
     */
    open fun retrieveImageData(context: Context) {
        val cache = File(context.cacheDir, Constants.IMAGE_DATA_CACHE_FILE)
        if (cache.exists()) {
            mDisposables.add(
                loadCache(context).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleCache) {
                        retrieveImageDataFromNetwork(context)
                    }
            )
        } else {
            retrieveImageDataFromNetwork(context)
        }

    }

    private fun retrieveImageDataFromNetwork(context: Context) {
        mDisposables.add(
            WebApiClient.instance.getImagesRx()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response: Response<Image> ->
                    val imageResult = response.body()?.mResult
                    Utils.saveToFile(File(context.cacheDir, Constants.IMAGE_DATA_CACHE_FILE), imageResult)
                    handleCache(imageResult)
                }, this::handleError)
        )
    }

    private fun handleCache(imageResult: ImageResult?) {
        val result = ArrayList<ImageData>()
        //process image data
        imageResult?.mImageInfos?.forEach { imageInfo ->
            imageInfo.getUrlList().forEach { url ->
                if (!TextUtils.isEmpty(url)) {
                    result.add(ImageData(imageInfo.mTitle, imageInfo.mMessage, url))
                }
            }
        }

        //save to data pool
        mCityGuideData.addAll(result.shuffled())
        mEatData.addAll(result.shuffled())
        mShopData.addAll(result.shuffled())
        EventBus.getDefault().postSticky(DataUpdateEvent())
    }

    private fun handleError(e: Throwable) {
        Log.w(TAG, "load Image Error", e)
    }

}