package com.timmy.tinkdemo.model.server

import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("result")
    val mResult: ImageResult?
)