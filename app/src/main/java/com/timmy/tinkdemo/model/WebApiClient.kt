package com.timmy.tinkdemo.model

import com.timmy.tinkdemo.model.server.Image
import com.timmy.tinkdemo.util.Constants
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

open class WebApiClient {

    private val mOpenDataApi by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.OPEN_DATA_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(OkHttpClient())
            .build()
            .create(WebApi::class.java)
    }

    private object Holder {
        val INSTANCE = WebApiClient()
    }

    companion object {
        var instance =  Holder.INSTANCE
    }

    open fun getImagesRx(): Single<Response<Image>> {
        return mOpenDataApi.getImageInfo()
    }

    interface WebApi {
        @GET("opendata/datalist/apiAccess?scope=resourceAquire&rid=f18de02f-b6c9-47c0-8cda-50efad621c14")
        fun getImageInfo(): Single<Response<Image>>
    }
}