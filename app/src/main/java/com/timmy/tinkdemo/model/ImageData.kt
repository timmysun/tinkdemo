package com.timmy.tinkdemo.model

data class ImageData(
    var mTitle: String?,
    var mMessage: String?,
    var mImageUrl: String?
)