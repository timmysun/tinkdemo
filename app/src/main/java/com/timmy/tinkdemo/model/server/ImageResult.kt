package com.timmy.tinkdemo.model.server

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ImageResult(
    @SerializedName("results")
    val mImageInfos: List<ImageInfo>? = null
) : Serializable {
    companion object {
        private const val serialVersionUID: Long = 876323262645176355L
    }
}