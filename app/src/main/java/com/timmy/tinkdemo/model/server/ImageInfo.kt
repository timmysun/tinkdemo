package com.timmy.tinkdemo.model.server

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ImageInfo(

    @SerializedName("F_Name_Ch")
    val mTitle: String?,

    @SerializedName("F_Brief")
    val mMessage: String?,

    @SerializedName("F_Pic01_URL")
    val mPic1Url: String?,

    @SerializedName("F_Pic02_URL")
    val mPic2Url: String?,

    @SerializedName("F_Pic03_URL")
    val mPic3Url: String?,

    @SerializedName("F_Pic04_URL")
    val mPic4Url: String?
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 876323262645176350L
    }

    fun getUrlList(): List<String?> {
        val list = ArrayList<String?>()
        list.add(mPic1Url)
        list.add(mPic2Url)
        list.add(mPic3Url)
        list.add(mPic4Url)
        return list
    }
}