package com.timmy.tinkdemo.helper

import android.content.Context
import android.content.DialogInterface
import android.databinding.DataBindingUtil
import android.support.v7.app.AlertDialog
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.timmy.tinkdemo.R
import com.timmy.tinkdemo.databinding.DialogQuiz1Binding
import com.timmy.tinkdemo.ui.viewmodel.Quiz1ViewModel

class Quiz1DialogHelper(context: Context) {

    //data binding
    private val mDialogQuiz1Binding by lazy {
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_quiz1, null, false) as DialogQuiz1Binding
    }

    //view model
    private val mViewModel = Quiz1ViewModel(context)

    //dialog builder
    private val mBuilder: AlertDialog.Builder = AlertDialog.Builder(context).setView(mDialogQuiz1Binding.root)

    //create dialog
    fun create(): AlertDialog {
        val dialog = mBuilder
            .setTitle(R.string.string_quiz1)
            .setCancelable(true)
            .create()

        //start button click listener
        mStartButton.setOnClickListener { _ ->
            mViewModel.calculateQuiz1(mEditInput.text.toString(), 100)
        }

        //set result scroll method
        mOutputText.movementMethod = ScrollingMovementMethod.getInstance()

        //bind data to result view
        mDialogQuiz1Binding.outputString = mViewModel.mOutput

        return dialog!!
    }

    private val mEditInput: EditText by lazy {
        mDialogQuiz1Binding.inputText
    }

    private val mStartButton: Button by lazy {
        mDialogQuiz1Binding.start
    }

    private val mOutputText: TextView by lazy {
        mDialogQuiz1Binding.output
    }

    fun onNegativeClickListener(func: () -> Unit): AlertDialog.Builder? =
        mBuilder.setNegativeButton(R.string.string_close) { _: DialogInterface, _: Int ->
            func()
        }
}