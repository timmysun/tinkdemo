package com.timmy.tinkdemo.util;

import java.util.ArrayList;
import java.util.List;

public class Quiz1Utils {

    static private final String PLUS = " + ";
    static private final String MINUS = " - ";

    /**
     * Recursive to combine the input string with +/- to leads express results to {@param value}
     * Notice that recursive may cause stack overflow so input string should not be too long.
     * We don't handle stack overflow here.
     *
     * @param input digit string for conbine with +/- to results to {@param value}
     * @param value result target value
     * @return A list of string that contains matched express
     */
    public static List<String> combineToValue(String input, long value) {
        ArrayList<String> result = new ArrayList<>();
        //separate sub string and recursive to combine the rest string
        //Ex. 123456789 ->
        // 1, 23456789
        // 12, 3456789
        // ...
        // 12345678, 9
        // due to first value can only be plus, so target value minus first value
        // and recursive to combine the rest string if target value results to 0
        for (int index = 1; index < input.length(); index++) {
            String curNumString = input.substring(0, index);
            long curNum = Long.parseLong(curNumString);
            String curRestString = input.substring(index);
            combineSubString(value - curNum, curRestString, String.valueOf(curNum), result);
        }
        return result;
    }

    private static void combineSubString(long value, String input, String prefix, List<String> result) {
        //if plus or minus input value causes target value results to 0, we get the answer
        long inputNum = Long.parseLong(input);
        String symbol = inputNum == value ? PLUS : (inputNum + value == 0 ? MINUS : null);

        //get answer, recursive stop
        if (symbol != null) {
            result.add(prefix + symbol + input);
        } else {
            // if input value length <= 1, won't get into for loop, recursive stop
            // do again separate current input to sub and recursive them.
            for (int index = 1; index < input.length(); index++) {
                long curNum = Long.parseLong(input.substring(0, index));
                String curRestString = input.substring(index);
                combineSubString(value - curNum, curRestString, prefix + PLUS + curNum, result);
                combineSubString(value + curNum, curRestString, prefix + MINUS + curNum, result);
            }
        }
    }
}
