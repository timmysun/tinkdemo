package com.timmy.tinkdemo.util

class Constants {
    companion object {
        const val OPEN_DATA_URL = "https://data.taipei/"
        const val IMAGE_DATA_CACHE_FILE = "image.cache"
    }
}