package com.timmy.tinkdemo.util

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.timmy.tinkdemo.R
import java.io.*

object Utils {

    /**
     * save Serializable to app cache folder
     */
    fun saveToFile(cache: File, result: Serializable?) {
        if (result != null) {
            try {
                val outputStream = FileOutputStream(cache)
                val objectOutputStream = ObjectOutputStream(outputStream)
                objectOutputStream.writeObject(result)
                objectOutputStream.close()
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * load serializable from app cache folder
     */
    fun loadFromFile(cache: File): Any? {
        if (cache.exists()) {
            try {
                val inputStream = FileInputStream(cache)
                val objectInputStream = ObjectInputStream(inputStream)
                val result = objectInputStream.readObject()
                objectInputStream.close()
                inputStream.close()
                return result
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return null
    }

    /**
     * load image to image view
     */
    @BindingAdapter("app:loadBigImage")
    @JvmStatic
    fun loadBigImage(imageView: ImageView, imageUrl: String) {
        Picasso.get()
            .load(imageUrl)
            .error(R.drawable.ic_error)
            .into(imageView)
    }
}